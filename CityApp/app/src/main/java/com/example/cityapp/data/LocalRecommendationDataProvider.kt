package com.example.cityapp.data

import com.example.cityapp.R
import com.example.cityapp.model.Recommendation

object LocalRecommendationDataProvider {
    val recommendations: List<Recommendation> = listOf(
        Recommendation(
            id = 1,
            name = R.string.university_1,
            category = R.string.category_1,
            address = R.string.university_address_1,
            description = R.string.university_desc_1,
            imageResourceId = R.drawable.university_1
        ),
        Recommendation(
            id = 2,
            name = R.string.university_2,
            category = R.string.category_1,
            address = R.string.university_address_2,
            description = R.string.university_desc_2,
            imageResourceId = R.drawable.university_2
        ),
        Recommendation(
            id = 3,
            name = R.string.university_3,
            category = R.string.category_1,
            address = R.string.university_address_3,
            description = R.string.university_desc_3,
            imageResourceId = R.drawable.university_3
        ),

        Recommendation(
            id = 4,
            name = R.string.culture_1,
            category = R.string.category_2,
            address = R.string.culture_address_1,
            description = R.string.culture_desc_1,
            imageResourceId = R.drawable.culture_1
        ),
        Recommendation(
            id = 5,
            name = R.string.culture_2,
            category = R.string.category_2,
            address = R.string.culture_address_2,
            description = R.string.culture_desc_2,
            imageResourceId = R.drawable.culture_2
        ),
        Recommendation(
            id = 6,
            name = R.string.culture_3,
            category = R.string.category_2,
            address = R.string.culture_address_3,
            description = R.string.culture_desc_3,
            imageResourceId = R.drawable.culture_3
        ),

        Recommendation(
            id = 7,
            name = R.string.parks_1,
            category = R.string.category_3,
            address = R.string.parks_address_1,
            description = R.string.parks_desc_1,
            imageResourceId = R.drawable.parks_1
        ),
        Recommendation(
            id = 8,
            name = R.string.parks_2,
            category = R.string.category_3,
            address = R.string.parks_address_2,
            description = R.string.parks_desc_2,
            imageResourceId = R.drawable.parks_2
        ),
        Recommendation(
            id = 9,
            name = R.string.parks_3,
            category = R.string.category_3,
            address = R.string.parks_address_3,
            description = R.string.parks_desc_3,
            imageResourceId = R.drawable.parks_3
        ),

        Recommendation(
            id = 10,
            name = R.string.coffee_1,
            category = R.string.category_4,
            address = R.string.coffee_address_1,
            description = R.string.coffee_desc_1,
            imageResourceId = R.drawable.coffee_1
        ),
        Recommendation(
            id = 11,
            name = R.string.coffee_2,
            category = R.string.category_4,
            address = R.string.coffee_address_2,
            description = R.string.coffee_desc_2,
            imageResourceId = R.drawable.coffee_2
        ),
        Recommendation(
            id = 12,
            name = R.string.coffee_3,
            category = R.string.category_4,
            address = R.string.coffee_address_3,
            description = R.string.coffee_desc_3,
            imageResourceId = R.drawable.coffee_3
        ),
    )
}